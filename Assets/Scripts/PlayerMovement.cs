﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 5;
    private Vector3 vector3;

    void Start()
    {

    }

    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
    }

    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Right")
        {
            transform.Rotate(0.0f, 270.0f, 0.0f, Space.World);
        }
    }
}
